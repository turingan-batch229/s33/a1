fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => json.map(({title}) => title))
.then((title) => console.log(title));




fetch("https://jsonplaceholder.typicode.com/todos/1").then((response) => response.json())
	.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos",
	{
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			title: "Created to Do List Item",
			complete: false,
			userID: 1
	})
})	
.then((response) => response.json())
.then((json) => console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "PUT",
		headers: {
			"Content-Type" : "application/json"
		},
			body: JSON.stringify({
				id: 1,
				title: "Updated To do List Item",
				status: "Pending",
				dateCompleted: "Pending",
				description: "To update the my to do list with a different data structure.",
				userID: 1
		})
})	
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "PATCH",
		headers: {
			"Content-Type" : "application/json"
		},
			body: JSON.stringify({
				status: "Complete",
				dateCompleted: "07/09/21"
		})
})	
.then((response) => response.json())
.then((json) => console.log(json));

https://jsonplaceholder.typicode.com/todos/1 
fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "DELETE",
});

